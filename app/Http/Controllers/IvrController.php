<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Services_Twilio_Twiml;

class IvrController extends Controller
{
    public function __construct()
    {
	$companyName = config('app.company.name');
        $this->_thankYouMessage = 'Thank you for calling the ' . $companyName.  ' phone';
    }

    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function showWelcome()
    {
        $response = new Services_Twilio_Twiml;
        $gather = $response->gather(
            ['numDigits' => 1,
             'action' => route('menu-response', [], false)]
        );

        $gather->play(
            'http://howtodocs.s3.amazonaws.com/et-phone.mp3',
            ['loop' => 3]
        );

        return $response;
    }

    /**
     * Responds to selection of an option by the caller
     *
     * @return \Illuminate\Http\Response
     */
    public function showMenuResponse(Request $request)
    {
        $selectedOption = $request->input('Digits');

        switch ($selectedOption)
        {
            case 1:
                return $this->_getReturnInstructions();
            case 2:
                return $this->_getIvrMenu();
        }

        $response = new Services_Twilio_Twiml;
        $response->say(
            'Returning to the main menu',
            ['voice' => 'Alice', 'language' => 'en-GB']
        );
        $response->redirect(route('welcome', [], false));

        return $response;
    }

    /**
     * Responds with a <Dial> to the caller's ivr
     *
     * @return \Illuminate\Http\Response
     */
    public function showIvrConnection(Request $request)
    {
        $response = new Services_Twilio_Twiml;
        $response->say(
            $this->_thankYouMessage,
            ['voice' => 'Alice', 'language' => 'en-GB']
        );
        $response->say(
            "You'll be connected shortly",
            ['voice' => 'Alice', 'language' => 'en-GB']
        );

        $ivrNumbers = [
            '2' => config('app.ivr.option.1.number'),
            '3' => config('app.ivr.option.2.number'),
            '4' => config('app.ivr.option.3.number')
        ];
        $selectedOption = $request->input('Digits');

        $ivrNumberExists = isset($ivrNumbers[$selectedOption]);

        if ($ivrNumberExists) {
            $selectedNumber = $ivrNumbers[$selectedOption];
            $response->dial($selectedNumber);

            return $response;
        } else {
            $errorResponse = new Services_Twilio_Twiml;
            $errorResponse->say(
                'Returning to the main menu',
                ['voice' => 'Alice', 'language' => 'en-GB']
            );
            $errorResponse->redirect(route('welcome', [], false));

            return $errorResponse;
        }

    }


    /**
     * Responds with instructions to mothership
     * @return Services_Twilio_Twiml
     */
    private function _getReturnInstructions()
    {
        $response = new Services_Twilio_Twiml;
	$returnText = config('app.ivr.return.instructions');
        $response->say($returnText,
            ['voice' => 'Alice', 'language' => 'en-GB']
        );
        $response->say(
            $this->_thankYouMessage,
            ['voice' => 'Alice', 'language' => 'en-GB']
        );

        $response->hangup();

        return $response;
    }

    /**
     * Responds with instructions to choose a ivr
     * @return Services_Twilio_Twiml
     */
    private function _getIvrMenu()
    {
        $response = new Services_Twilio_Twiml;
        $gather = $response->gather(
            ['numDigits' => '1', 'action' => route('ivr-connection', [], false)]
        );
	$option1 = config('app.ivr.option.1');
	$option1key= config('app.ivr.option.1.key');
	$option2 = config('app.ivr.option.2');
   	$option2key = config('app.ivr.option.2.key');
	$option3 = config('app.ivr.option.3');
        $option3key = config('app.ivr.option.key.3');
	$text = "To call $option1, press $option1key. To call $option2, press $option2key. To return press $option3key";

        $gather->say(
	    $text,
            ['voice' => 'Alice', 'language' => 'en-GB']
        );

        return $response;
    }
}
